package com.taco.enity;

import lombok.Data;
import org.hibernate.validator.constraints.CreditCardNumber;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "taco_order")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Date placedAt;


    @NotBlank(message = "名稱不能為空")
    private String deliveryName;
    @NotBlank(message = "街道不能為空")
    private String deliveryStreet;
    @NotBlank(message = "城市不能為空")
    private String deliveryCity;
    @NotBlank(message = "州不能為空")
    private String deliveryState;
    @NotBlank(message = "郵遞區號不能為空")
    private String deliveryZip;

    @CreditCardNumber(message = "信用卡帳號錯誤")
    private String ccNumber;

    @Pattern(regexp = "^(0[1-9]|1[0-2])([\\/])([1-9][0-9])$",
            message = "日期需依照 MM/YY")
    private String ccExpiration;
    @Digits(integer = 3, fraction = 0, message = "驗證碼錯誤")
    private String ccCVV;

    @ManyToMany(targetEntity = Taco.class)
    private List<Taco> tacoList = new ArrayList<>();


    public void addTaco(Taco taco) {
        this.tacoList.add(taco);
    }

    @PrePersist
    void placedAt() {
        this.placedAt = new Date();
    }
}
