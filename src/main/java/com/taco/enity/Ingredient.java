package com.taco.enity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * 配料
 */
@Data   // 生成所有getter.setter
@RequiredArgsConstructor // 生成所有final變數的構造器
@NoArgsConstructor(access = AccessLevel.PUBLIC,force = true)
@Entity
public class Ingredient {

    @Id
    private final String id;
    // 配料名稱
    private final String name;
    // 配料種類
    private final Type type;

    public static enum Type {
        WRAP, PROTEIN, VEGGIES, CHEESE, SAUCE
    }
}
